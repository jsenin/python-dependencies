# python-dependencies

## A POC for ilustrating how install recursive depedencies using pip and python

I built an example of requirement dependencies to three nodes, go to module_c and try to run:
```
cd module_c
pip install -r requirements.txt
python run_fetch.py
```

## Leasson learned

* use setuptools over disutils 
* setuptools works well with pypi packages but not using git urls or local paths
* pip install reads setup.py or pyproject.toml files to install recursive dependencies

* must to read https://caremad.io/posts/2013/07/setup-vs-requirement/

### setup.py versus requirements.txt
* must to read https://caremad.io/posts/2013/07/setup-vs-requirement/
* more https://packaging.python.org/discussions/install-requires-vs-requirements/


> First, requirements.txt and setup.py are for two separate use-cases and probably shouldn't be used together in most cases. You can read Donald Stufft's post on this, but basically setup.py is for specifying a library's dependencies while requirements.txt is for specifying your app's dependencies.
> https://snarky.ca/clarifying-pep-518/

> A Python library in this context is something that has been developed and released for others to use. 
> https://snarky.ca/clarifying-pep-518/

> that setup.py is designed for redistributable things and that requirements.txt is designed for non-redistributable things ...
> https://snarky.ca/clarifying-pep-518/

> It’s important to be clear that pip determines package dependencies using install_requires metadata, not by discovering requirements.txt files embedded in projects.
> https://pip.pypa.io/en/stable/user_guide/#installing-packages


### pyproject.toml
* PEP 517 -- A build-system independent format for source trees https://www.python.org/dev/peps/pep-0517/
* PEP 518 -- Specifying Minimum Build System Requirements for Python Projects https://www.python.org/dev/peps/pep-0518/

### Package dependencies
* pep-508 Dependency specification for Python Software Packages https://www.python.org/dev/peps/pep-0508/

### packaging for pypi
* https://pythonwheels.com/
* https://flit.readthedocs.io/en/latest/index.html
