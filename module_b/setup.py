# based on https://raw.githubusercontent.com/pypa/sampleproject/master/setup.py

from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

with open('{}/requirements.txt'.format(here)) as f:
    required = f.read().splitlines()

# https://mike.zwobble.org/2013/05/adding-git-or-hg-or-svn-dependencies-in-setup-py/
setup(
    name='module_b',
    version='0.0.1',
    description='A module_b that uses module_a for requesting urls',
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    install_requires=['module_a'],
    dependency_links=['git+git://gitlab.com:jsenin/python-dependencies.git#egg=module_a']
)
