# based on https://raw.githubusercontent.com/pypa/sampleproject/master/setup.py

from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

with open('{}/requirements.txt'.format(here)) as f:
    required = f.read().splitlines()

setup(
    name='module_a',
    version='0.0.1',
    description='A module for requesting urls',
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    install_requires=required
)
